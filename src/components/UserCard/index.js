import { useDispatch, useSelector } from "react-redux";
import { useState } from 'react'
import { changeName } from "../../store/modules/user/actions";

const UserCard = () => {
    const dispatch = useDispatch();
    const test = useSelector((state) => state.user)
    const [input, setInput] = useState("");

    const handleClick = (newName) => {
        dispatch(changeName(newName));
    }

    return (
        <div>
            <input onChange={(e) => setInput(e.target.value)} type="text" />
            <button onClick={() => handleClick(input)}>Change</button>
            <div>
                {test.name}
            </div>
        </div>
    )
}

export default UserCard;